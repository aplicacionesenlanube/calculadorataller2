// ajuste del resultado

function setResult(value) {
    document.getElementById('result').innerHTML = value;
}
//obtener el resultado
function getResult() {
    return(document.getElementById('result').innerHTML);
}
// función sumar
function add(key) { 
    var result = getResult();
    if (result!='0' || isNaN(key)) setResult(result + key);
    else setResult(key);
}
// función calcular
function calc() {
    var result = eval(getResult()); 
    setResult(result);
}
// función borrar
function del() { 
    setResult(0);
}
